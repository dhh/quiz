loadPage();

// load page
function loadPage(){
    var pageContent = document.getElementById("page-content");
    if (!localStorage.numPage || localStorage.numPage == "0"){
        localStorage.numPage = "0";
        document.title = "Registration Form";
        pageContent.innerHTML = '<object type="text/html" data="registration-form.html" class="object-page" id="registration-page"></object>';
    }
    else {
        var questions = JSON.parse(localStorage.questions);
        var numPage = JSON.parse(localStorage.numPage);
        var input = JSON.parse(localStorage.input);
        if (numPage == questions.length + 1) {
            document.title = "Result";
            pageContent.innerHTML = '<object type="text/html" data="result-page.html" class ="object-page" id="result-page"></object>';
        }
        else {
            document.title = "Question " + numPage;
            pageContent.innerHTML = '<object type="text/html" data="single-question.html" class="object-page" id="single-question-page"></object>';
            // loadQuestion(questions[numPage - 1], numPage);
            // loadInput(input[numPage - 1]);
        }
    }
}


// function loadQuestion(q, num){
//     $("#ques").innerHTML = "Question " + num + ": "+ q.question;
//     $("#ans1").innerHTML = "A. " + q.ansA;
//     $("#ans2").innerHTML = "B. " + q.ansB;
//     $("#ans3").innerHTML = "C. " + q.ansC;
//     $("#ans4").innerHTML = "D. " + q.ansD;
// }


function loadInput(input){
    // for (var i = 0; i < 4; ++i){
    //     if (i < )
    // }
}



//Menu bar control 
$("#menu-bar-control").click(showHideMenuBar);

function showHideMenuBar(){
    var menu = $("#menu-bar");
    var pageContent = $("#page-content");
    if (menu.css("position") == "fixed"){
        if (menu.css("display") != "block"){
            menu.css("display","block");    
            menu.css("margin-left", "0px")
            pageContent.css("margin-left", "275px");
            return;
        }
        if (menu.css("margin-left") == "0px") {
            menu.css("margin-left", "-100%");
            pageContent.css("margin-left", "0px");
        }
        else {
            menu.css("margin-left", "0px");
            pageContent.css("margin-left", "275px");
        }
    }
    else {
        if (menu.css("margin-left") != "0px"){
            menu.css("margin-left", "0px");
            menu.css("display","block");
            return;
        }
        if (menu.css("display") == "block"){
            menu.css("display", "none");
        }
        else {
            menu.css("display","block");
        }

    }
}


