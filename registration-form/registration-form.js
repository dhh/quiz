document.getElementById("upload-button").addEventListener("click",function(){
    document.getElementById("upload-profile-pic").click();
});
document.getElementById("upload-profile-pic").addEventListener("change", upload);
document.getElementById("submit-button").addEventListener("click",submit)



function initLocalStorage(){
    localStorage.clear();
    localStorage.numPage = 0;
    localStorage.nQuestion = 5;
    localStorage.timePerQuestion = 10;
    localStorage.questions = JSON.stringify([
    {
        "single":true,
        "question":"1+1?",
        "ans1":"1",
        "ans2":"2",
        "ans3":"3",
        "ans4":"4",
        "answer":"0100"},
    {
        "single":false,
        "question":"digit?",
        "ans1":"1",
        "ans2":"a",
        "ans3":"4",
        "ans4":"b",
        "answer":"1010"},
    {
        "single":true,
        "question":"2^2?",
        "ans1":"1",
        "ans2":"8",
        "ans3":"4",
        "ans4":"12",
        "answer":"0010"},
    {
        "single": false,
        "question": "letter?",
        "ans1": "a",
        "ans2": "b",
        "ans3": "4",
        "ans4": "12",
        "answer": "1100"},
    {
        "single":true,
        "question":"year?",
        "ans1":"2014",
        "ans2":"2015",
        "ans3":"2016",
        "ans4":"2017",
        "answer":"0001"}]);
    var input = [];
    for (var i = 0; i < JSON.parse(localStorage.nQuestion); ++i){
        input.push({answer:"0000", time:0});
    }
    localStorage.input = JSON.stringify(input);
}

initLocalStorage();


var profilePicStatus = false;


function upload(){
    getPicture();
    profilePicStatus = true;
    // savePicture();
}

function getPicture(){
    var linkImg = document.getElementById("upload-profile-pic").files[0];
    if (!linkImg) return;
    linkImg = window.URL.createObjectURL(linkImg);

    var image = document.getElementById("profile-pic");
    image.src = linkImg;

}

function savePicture(){
    var image = document.getElementById("profile-pic");

    var imgCanvas = document.createElement("canvas"),
        imgContext = imgCanvas.getContext("2d");
    
    imgCanvas.width = image.width;
    imgCanvas.height = image.height;

    imgContext.drawImage(image, 0, 0, image.width, image.height);

    var imgAsDataURL = imgCanvas.toDataURL("image/png");

    localStorage.profilePic = imgAsDataURL;
}

function validName(name){
    name = name.split(" ").filter(function(c){return c!=""}).join(' ');

    var nameError = document.getElementById("input-name-error");
    nameError.style.display = "inline-block";
    var nameClone = document.getElementById("name-clone-property");
    nameClone.style.display = "inline-block";
    document.getElementById("input-name").style.border = "1px solid red";
    
    if (name.length == 0){
        nameError.innerHTML = "this field is required";
        return false;
    }

    if (name.length < 8){
        nameError.innerHTML = "your name's length must be at least 8";
        return false;
    }
    if (name.match(/(?:^| )[a-z]/))
    {
        nameError.innerHTML = "your name must be capitalized";
        return false;
    }    

    document.getElementById("input-name").style.border = "1px solid green";
    nameError.style.display = "none";
    nameClone.style.display = "none";
    localStorage.inputName = name;
    document.getElementById("input-name").value = name;
    return true;
}


function validDOB(date){
    var dobError = document.getElementById("input-dob-error");
    var dobClone = document.getElementById("dob-clone-property");
    
    if (!date){
        dobError.style.display = "inline-block";
        dobClone.style.display = "inline-block";
        document.getElementById("input-dob").style.border = "1px solid red";
        return false;
    }

    dobError.style.display = "none";
    dobClone.style.display = "none";
    document.getElementById("input-dob").style.border = "1px solid green";
    localStorage.inputDOB = date;
    return true;

}


function saveCity(city){
    localStorage.inputCity = city;
    document.getElementById("input-city").style.border = "1px solid green";
}


function validGender(gender){
    var genderError = document.getElementById("input-gender-error");
    var genderClone = document.getElementById("gender-clone-property");
   
    if (!gender){
        genderError.style.display = "inline-block";
        genderClone.style.display = "inline-block";
        document.getElementById("male-label").style.color = "red";
        document.getElementById("female-label").style.color = "red";
        return false;
    }

    genderError.style.display = "none";
    genderClone.style.display = "none";
    document.getElementById("male-label").style.color = "black";
    document.getElementById("female-label").style.color = "black";
    if (gender.id == "male"){
        document.getElementById("male-label").style.color = "green";
        localStorage.inputGender = "male";
    } else{
        document.getElementById("female-label").style.color = "green";
        localStorage.inputGender = "female";
    }
    return true;
}


function validEmail(email){
    email = email.trim();
    
    var emailError = document.getElementById("input-email-error");
    emailError.style.display = "inline-block";
    var emailClone = document.getElementById("email-clone-property");
    emailClone.style.display = "inline-block";
    document.getElementById("input-email").style.border = "1px solid red";

    if (email.length == 0){
        emailError.innerHTML = "this feild is required";
        return false;
    }
    if (!email.match(/^([a-z0-9]+\.)*[a-z0-9]+@[a-z]+\.([a-z]+)\.*[a-z]+$/)){
        emailError.innerHTML = "your email is not valid";
        return false;
    }

    document.getElementById("input-email").style.border = "1px solid green";
    emailError.style.display = "none";
    emailClone.style.display = "none";
    localStorage.inputEmail = email;
    document.getElementById("input-email").value = localStorage.inputEmail;
    return true;
}


function saveDepartment(department){
    var arr = [];
    localStorage.inputDepartment = "";
    for (var i = 0; i < department.length; ++i){
        if (department[i].checked){
            document.getElementById("label-" + department[i].id).style.color = "green";
            arr.push(department[i].id);
        }
        else {
            document.getElementById("label-" + department[i].id).style.color = "black";
        }
    }
    localStorage.inputDepartment = JSON.stringify(arr);
}


function validProfilePic(){
    if (!profilePicStatus){
        document.getElementById("profile-pic-error").style.display = "block";
        document.getElementById("profile-pic").style.border = "5px solid red";
        return false;
    } 
    else{
        savePicture();
        document.getElementById("profile-pic-error").style.display = "none";
        document.getElementById("profile-pic").style.border = "5px solid rgb(221,221,221)";
        return true;
    }
}


function alertInformations(){
    var department = document.querySelectorAll('input[type="checkbox"]:checked');
    var message ="Name: " + localStorage.inputName + "<br>" + "DOB: " + localStorage.inputDOB + "<br>"
    + "City: " + localStorage.inputCity + "<br>" + "Gender: " + localStorage.inputGender + "<br>" +
    "Email: " + localStorage.inputEmail + "<br>" + "Department: ";
    var department = JSON.parse(localStorage.inputDepartment);

    for (var i = 0; i < department.length; ++i)
    {
        if (i == 0)  message += department[i];
        else message += ", " + department[i];
    } 
    if (department.length == 0) message += "none";
    
    //alert(message);
    $("#title").html("Registered Successfully");
    $("#no-button").css("display","none");
    $("#yes-button").html("OK");
    $(".form-content").click(false);
    $(".form-content :input").attr("disabled", true);
    $("#content").html(message);
    $("#no-button").unbind('click');
    $("#yes-button").unbind('click');
    $("#yes-button").click(function(){
        $("#modal").css("display","none");
        nextPage();
    });
    $("#close").click(function(){
        $("#modal").css("display","none");
        nextPage();
    });
    $("#modal").css("display","block");
}


function submit(){
    var checkName = validName(document.getElementById("input-name").value);
    var checkDOB = validDOB(document.getElementById("input-dob").value);
    saveCity(document.getElementById("input-city").value);
    var checkGender = validGender(document.querySelector('input[name="gender"]:checked'));
    var checkEmail = validEmail(document.getElementById("input-email").value);
    saveDepartment(document.querySelectorAll('input[type="checkbox"]'));
    var checkProfilePic = validProfilePic();
    if (checkName && checkDOB && checkGender && checkEmail && checkProfilePic) {
        alertInformations();
        // nextPage();
    }
}


function nextPage(){
    localStorage.time = JSON.parse(localStorage.timePerQuestion) * JSON.parse(localStorage.nQuestion);
    localStorage.numPage = JSON.parse(localStorage.numPage) + 1;
    var questions = JSON.parse(localStorage.questions);
    var numPage = JSON.parse(localStorage.numPage);
    if (questions[numPage - 1].single == true){
        window.location.href = "../single-question/single-question.html";
    }
    else {
        window.location.href = "../multiple-question/multiple-question.html";
    }
}