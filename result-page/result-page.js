var time_1 = 50;
var time_2 = 55;
var time_3 = 40;
var time_4 = 25;


google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);


function getData(){
  var output = [ ["Question", "Time", { role: "style" }] ];
  var input = JSON.parse(localStorage.input);
  var questions = JSON.parse(localStorage.questions);
  var nQuestion = JSON.parse(localStorage.nQuestion);
  for (var i = 0; i < nQuestion; ++i){
    var arr = [];
    arr.push(JSON.stringify(i + 1));
    arr.push(input[i].time);
    if (input[i].answer == questions[i].answer){
      arr.push("green");}
    else {
      arr.push("red");
    }
    output.push(arr);
  }
  return output;
}


function drawChart() {
  // var data = google.visualization.arrayToDataTable([
  //   ["Question", "Time", { role: "style" } ],
  //   ["1", time_1, "#b87333"],
  //   ["2", time_2, "silver"],
  //   ["3", time_3, "gold"],
  //   ["4", time_4, "color: #e5e4e2"]
  // ]);
  var data = google.visualization.arrayToDataTable(getData());

  var view = new google.visualization.DataView(data);
  view.setColumns([0, 1,
                   { calc: "stringify",
                     sourceColumn: 1,
                     type: "string",
                     role: "annotation" },
                   2]);

  var widthOfChart = $("#result-chart").css("width");
  var options = {
    title: "Time, in second",
    // width: widthOfChart,
    // height: 280,
    bar: {groupWidth: "40%"},
    legend: { position: "none" },
  };
  var chart = new google.visualization.ColumnChart(document.getElementById("result-chart"));
  chart.draw(view, options);
}

function countTrueAnswers(){
  var trueAnswers = 0;
  var questions = JSON.parse(localStorage.questions);
  var input = JSON.parse(localStorage.input);
  var nQuestion = JSON.parse(localStorage.nQuestion);
  for (var i = 0; i < nQuestion; ++i)
    if (questions[i].answer == input[i].answer) ++trueAnswers;
  return trueAnswers;
}

function loadProfilePic(){
  var img = document.getElementById("profile-pic");
  img.src = localStorage.profilePic;
}

//content
loadProfilePic();
document.getElementById('name').innerHTML = localStorage.inputName;
document.getElementById("class").innerHTML = 'Real Web';
document.getElementById("test-done").innerHTML = '3/4';

//result-content
document.getElementById("point").innerHTML = ((countTrueAnswers() / JSON.parse(localStorage.nQuestion)) * 10).toFixed(2);
document.getElementById("time").innerHTML = JSON.parse(localStorage.nQuestion) * JSON.parse(localStorage.timePerQuestion) - JSON.parse(localStorage.time);
document.getElementById("true-answers").innerHTML = countTrueAnswers() + " / " + localStorage.nQuestion;