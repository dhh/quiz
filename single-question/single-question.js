$('body').css('display', 'none');
$(document).ready(function(){
    if (localStorage.numPage == "1"){
        $("#back-button").css("display","none");
    }
    if (localStorage.numPage == localStorage.nQuestion){
        document.getElementById("next-button").src = "pictures/submit-button.png";
        $("#next-button").css("width","auto");
    }
    loadQuestionAnswer();
    loadUserInput();    
    loadProgressBar();
    $('body').fadeIn(1000);
    setInterval(function(){
        var time = JSON.parse(localStorage.time);
        var input = JSON.parse(localStorage.input);
        var numPage = JSON.parse(localStorage.numPage) - 1;
        if (time == 0){
            $("#title").html("Time's up");
            $("#content").html("Time is over. Go to result page.")
            $("#no-button").css("display","none");
            $("#yes-button").html("OK");
            $("#time").css("display","none");
            $(".answers").click(false);
            $(".answers :input").attr("disabled", "true");
            $("#yes-button").unbind('click');
            $("#close").unbind('click');
            $("#yes-button").click(function(){
                window.location.href = "../result-page/result-page.html";
            });
            $("#close").click(function(){
                window.location.href = "../result-page/result-page.html";
            });
            $("#back-button").unbind('click');
            $("#next-button").unbind('click');
            $("#modal").css("display","block");
        }
        var minute = Math.floor(time / 60);
        var second = Math.floor(time % 60);
        if (minute < 10) minute = "0" + minute;
        if (second < 10) second = "0" + second;
        document.getElementById("time").innerHTML = minute + "m" + " " + second + "s";
        --time;
        if (time >= 0) {
            input[numPage].time++;
            localStorage.time = time;
        }
        localStorage.input = JSON.stringify(input);
    },1000)
})


function loadQuestionAnswer(){
    var numPage = JSON.parse(localStorage.numPage) - 1;
    var question = JSON.parse(localStorage.questions)[numPage];
    document.getElementById("ques").innerHTML = "Question " + (numPage + 1) + ": " + question.question;
    document.getElementById("ans1").innerHTML = "A. " + question.ans1;
    document.getElementById("ans2").innerHTML = "B. " + question.ans2;
    document.getElementById("ans3").innerHTML = "C. " + question.ans3;
    document.getElementById("ans4").innerHTML = "D. " + question.ans4;
}

function loadUserInput(){
    var numPage = JSON.parse(localStorage.numPage) - 1;
    var input = JSON.parse(localStorage.input);
    if (!input[numPage]){
        input.push({answer: "0000", time: 0});
        localStorage.input = JSON.stringify(input);
    }
    input = input[numPage];
    var answer = input.answer;
    for (var i = 1; i <= 4; ++i){
        document.getElementById("radio" +  i).checked = (answer[i - 1] == "1");
    }
}

function passedQuestion(){
    var input = JSON.parse(localStorage.input);
    var passed = 0;
    for (var i = 0; i < input.length; ++i)
    {
        if (input[i].answer != "0000") ++passed;
    }
    return passed;
}


function firstUndoneQuestion(){
    var input = JSON.parse(localStorage.input);
    for (var i = 0; i < input.length; ++i)
    {
        if (input[i].answer == "0000") return i + 1;
    }
}


function loadProgressBar(){
    var nQuestion = JSON.parse(localStorage.nQuestion);
    var passed = passedQuestion();
    document.getElementById("percent").innerHTML = passed + " / " + nQuestion;
    var width = ((passed / nQuestion) * 100) + "%";
    $("#loading-bar").css("width",width)
}


function saveAnswer(){
    var numPage = JSON.parse(localStorage.numPage);
    var input = JSON.parse(localStorage.input);
    var answer = "";
    for (var i = 1; i <= 4; ++i)
    {
        if (document.getElementById("radio" + i).checked) answer +="1";
        else answer += "0";
    }
    input[numPage - 1].answer = answer;
    localStorage.input = JSON.stringify(input);
}


function backFunction(){
    saveAnswer();
    var numPage = JSON.parse(localStorage.numPage) - 1;
    var questions = JSON.parse(localStorage.questions);
    localStorage.numPage = numPage;
    $('body').fadeOut(1000);
    if (questions[numPage - 1].single == true){
        window.location.href = "./single-question.html";
    }
    else {
        window.location.href = "../multiple-question/multiple-question.html";
    }
}

$("#back-button").click(backFunction);


function nextFunction(){
    saveAnswer();
    var numPage = JSON.parse(localStorage.numPage);
    var questions = JSON.parse(localStorage.questions);
    var nQuestion = JSON.parse(localStorage.nQuestion);
    if (numPage == nQuestion){
        var passed = passedQuestion();
        if (passed == nQuestion){
            $("#title").html("Submit");
            $("#content").html("Are you sure to submit?")
            $("#no-button").html("No");
            $("#yes-button").html("Yes");
            $(".answers").click(false);
            $(".answers :input").attr("disabled", "true");
            $("#no-button").unbind('click');
            $("#yes-button").unbind('click');
            $("#close").unbind('click');
            $("#yes-button").click(function(){
                window.location.href = "../result-page/result-page.html";
            });
            $("#no-button").click(function(){
                $("#modal").css("display","none");
                $(".answers").unbind('click');
                $(".answers :input").removeAttr("disabled")
                $("#back-button").click(backFunction);
                $("#next-button").click(nextFunction);
            })
            $("#close").click(function(){
                $("#modal").css("display","none");
                $(".answers").unbind('click');
                $(".answers :input").removeAttr("disabled")
                $("#back-button").click(backFunction);
                $("#next-button").click(nextFunction);
            })
            $("#back-button").unbind('click');
            $("#next-button").unbind('click');
            $("#modal").css("display","block");
        }
        else 
        {
            $("#title").html("Submit");
            $("#content").html("You have completed " + passed + " / " + nQuestion + " questions. Are you sure to submit?");
            $("#no-button").html("No");
            $("#yes-button").html("Yes");
            $(".answers").click(false);
            $(".answers :input").attr("disabled", "true");
            $("#no-button").unbind('click');
            $("#yes-button").unbind('click');
            $("#close").unbind('click');
            $("#yes-button").click(function(){
                window.location.href = "../result-page/result-page.html";
            });
            $("#no-button").click(function(){
                $("#modal").css("display","none");
                $(".answers").unbind('click');
                $(".answers :input").removeAttr("disabled");
                $("#back-button").click(backFunction);
                $("#next-button").click(nextFunction);
                numPage = firstUndoneQuestion();
                localStorage.numPage = numPage;
                if (questions[numPage - 1].single == true){
                    window.location.href = "./single-question.html";
                }
                else {
                    window.location.href = "../multiple-question/multiple-question.html";
                }
            })
            $("#close").click(function(){
                $("#modal").css("display","none");
                $(".answers").unbind('click');
                $(".answers :input").removeAttr("disabled")
                $("#back-button").click(backFunction);
                $("#next-button").click(nextFunction);
                numPage = firstUndoneQuestion();
                localStorage.numPage = numPage;
                if (questions[numPage - 1].single == true){
                    window.location.href = "./single-question.html";
                }
                else {
                    window.location.href = "../multiple-question/multiple-question.html";
                }
            })
            $("#back-button").unbind('click');
            $("#next-button").unbind('click');
            $("#modal").css("display","block");
        }
    }
    else{
        localStorage.numPage = ++numPage;
        if (questions[numPage - 1].single == true){
            window.location.href = "./single-question.html";
        }
        else {
            window.location.href = "../multiple-question/multiple-question.html";
        }
    }
}


$("#next-button").click(nextFunction);

$("input").click(function(){
    saveAnswer();
    loadProgressBar();
})